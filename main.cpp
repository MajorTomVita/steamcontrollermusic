#include <iostream>
#include <stdint-gcc.h>
#include <unistd.h>
#include <stdint.h>
#include <cstdlib>
#include <pthread.h>

#include "libusb/libusb.h"
#include "libusb/MidiFile.h"

#include "midi.h"

#define STEAM_CONTROLLER_MAGIC_PERIOD_RATIO 495483.0

using namespace std;

int g_job0_done = 0;
int g_job1_done = 0;

typedef struct {
    libusb_device_handle *dev_handle;
    noteStruct *noteArray;
    int notesCount;
    int channel;
    int isWireless;
} threadArgs_t;

void delay_ms(unsigned int ms){
    for(; ms > 0 ; ms --)
        usleep(1000);
}

noteStruct *generate_mid_array(const char *file, int channel, int *noteCount)
{
    double totalTime = 0;
    
    noteStruct *noteArray = NULL;
	MidiFile midifile;
	int evt, track;
	
	midifile.read(file);
    midifile.linkNotePairs();
    midifile.doTimeAnalysis();
    midifile.joinTracks();
	
	noteArray = (noteStruct *)malloc(sizeof(noteStruct) * midifile.getEventCount(0));
    
    *noteCount = 0;
    
	for (track = 0; track < midifile.getTrackCount(); track++)
	{
		for (evt = 0; evt < midifile[track].size(); evt++) 
		{   
            if (!midifile[track][evt].isNoteOn())
                continue;
                        
            if ((int)midifile[track][evt].getChannel() == channel) {
                if (totalTime + midifile[track][evt].getDurationInSeconds() < midifile[track][evt].seconds) {
                    cout << "Added " << (unsigned int)((midifile[track][evt].seconds * 1000) - (totalTime * 1000)) << " ms of silence" << endl;
                    noteArray[*noteCount].note = 0;
                    noteArray[*noteCount].delay = (unsigned int)((midifile[track][evt].seconds * 1000) - (totalTime * 1000));
                    *noteCount = *noteCount + 1;
                    totalTime = midifile[track][evt].seconds;
                }
                noteArray[*noteCount].note = (unsigned int)midifile[track][evt][1];
                noteArray[*noteCount].delay = (unsigned int)(midifile[track][evt].getDurationInSeconds() * 1000);
                totalTime += midifile[track][evt].getDurationInSeconds();
                *noteCount = *noteCount + 1;
            }
      	}
	}
    
    return noteArray;
}

void find_channels(const char *filename, int channels[])
{
    int chan[16] = {0};
    MidiFile midifile;
    
    midifile.read(filename);
    midifile.linkNotePairs();
    midifile.doTimeAnalysis();
    //midifile.joinTracks();
    
    for (int track = 0; track < midifile.getTrackCount(); track++)
	{
		for (int evt = 0; evt < midifile[track].size(); evt++) 
		{   
            if (!midifile[track][evt].isNoteOn())
                continue;
                
            chan[midifile[track][evt].getChannel()] = 1;
            
            if (channels[0] == -1)
                channels[0] = (int)midifile[track][evt].getChannel();
            else if (channels[0] != (int)midifile[track][evt].getChannel()) {
                channels[1] = (int)midifile[track][evt].getChannel();
                break;
            }
        }
    }
    int nChan = 0;
    for (int i = 0; i < 16; i++)
    {
        if (chan[i] == 1)
            nChan++;
    }
    cout << midifile.getTrackCount() << " tracks found : ";
    for (int i = 0; i < 16; i++)
    {
        if (chan[i] == 1)
            cout << i << ", ";
    }
    cout << endl;

    if (channels[1] == -1)
        channels[1] = channels[0];
}

int playNoteOnSteamController(libusb_device_handle *dev_handle, int isWireless, unsigned int note, unsigned int delay, unsigned char channel){
    unsigned char dataBlob[64] = {0x8f,
                                  0x07,
                                  0x00, //Trackpad select : 0x01 = left, 0x00 = right
                                  0xff, //LSB Pulse High Duration
                                  0xff, //MSB Pulse High Duration
                                  0xff, //LSB Pulse Low Duration
                                  0xff, //MSB Pulse Low Duration
                                  0xff, //LSB Pulse repeat count
                                  0x04, //MSB Pulse repeat count
                                  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    double frequency = midiFrequency[note];
    double period = 1.0 / frequency;
    uint16_t periodCommand = period * STEAM_CONTROLLER_MAGIC_PERIOD_RATIO;

    double duration = delay / 1000.0;
    uint16_t repeatCount = duration / period;

    cout << "Frequency : " << frequency << ", Period : " << periodCommand << ", Repeat : " << repeatCount << ", Channel : " << (int)channel << "\n";

    dataBlob[2] = channel;
    
    dataBlob[3] = periodCommand % 0xff;
    dataBlob[4] = periodCommand / 0xff;
    dataBlob[5] = periodCommand % 0xff;
    dataBlob[6] = periodCommand / 0xff;
    dataBlob[7] = repeatCount % 0xff;
    dataBlob[8] = repeatCount / 0xff;

    int r;
    r = libusb_control_transfer(dev_handle,0x21,9,0x0300,isWireless ? 1 : 2,dataBlob,64,1000);
    if(r < 0) {
        cout<<"Command Error "<<r<<endl;
        std::cin.ignore();
        return 1;
    }

    return 0;
}

void *playLoop(void *thread_args)
{
    threadArgs_t *args = (threadArgs_t *)thread_args;
    
    //if (args->channel == 0) return NULL;
    while (1)
    for(int i = 0 ; i < args->notesCount ; i++){
        if(args->noteArray[i].note != 0){
            int r = playNoteOnSteamController(args->dev_handle, args->isWireless, args->noteArray[i].note, args->noteArray[i].delay, args->channel);
            if(r != 0) {
                if (args->channel == 0) g_job0_done = 1;
                if (args->channel == 1) g_job1_done = 1;
                pthread_exit(NULL);
            }
        }
        delay_ms(args->noteArray[i].delay);
    }
    if (args->channel == 0) g_job0_done = 1;
    if (args->channel == 1) g_job1_done = 1;
    pthread_exit(NULL);
    return NULL;
}

int main(int argc, char *argv[])
{
    char *filename = argv[1];
    pthread_t threads[2];
    threadArgs_t args[2];
    
    int isWireless = 0;
    int notesCount0, notesCount1;
    int channels[2] = {-1, -1};
    find_channels(filename, channels);
    
    noteStruct *noteArray0 = generate_mid_array(filename, channels[0], &notesCount0);
    noteStruct *noteArray1 = generate_mid_array(filename, channels[1], &notesCount1);
    
    libusb_device_handle *dev_handle; //a device handle
    libusb_context *ctx = NULL; //a libusb session
    int r; //for return values
    int interface_num = 0;

    cout <<"Steam Controller Singer by Pila"<<endl;

    //Initializing LIBUSB
    r = libusb_init(&ctx);
    if(r < 0) {
        cout<<"Init Error "<<r<<endl;
        std::cin.ignore();
        return 1;
    }

    libusb_set_debug(ctx, 3);

    //Open Steam Controller device
    if((dev_handle = libusb_open_device_with_vid_pid(ctx, 0x28DE, 0x1102)) != NULL){ // Wired Steam Controller
        cout<<"Found wired Steam Controller"<<endl;
        interface_num = 2;
    }
    else if((dev_handle = libusb_open_device_with_vid_pid(ctx, 0x28DE, 0x1142)) != NULL){ // Steam Controller dongle
        cout<<"Found Steam Dongle, will attempt to use the first Steam Controller"<<endl;
        interface_num = 1;
        isWireless = 1;
    }
    else{
        cout<<"No device found"<<endl;
        std::cin.ignore();
        return 1;
    }

    //On Linux, automatically detach and reattach kernel module
    libusb_set_auto_detach_kernel_driver(dev_handle,1);

    //Claim the USB interface controlling the haptic actuators
    r = libusb_claim_interface(dev_handle,interface_num);
    if(r < 0) {
        cout<<"Interface claim Error "<<r<<endl;
        std::cin.ignore();
        return 1;
    }

    cout<<"Ready. Press Enter to start"<<endl;
    std::cin.ignore();

    //Create thread for each channel
    args[0].dev_handle = dev_handle;
    args[0].noteArray = noteArray0;
    args[0].notesCount = notesCount0;
    args[0].channel = 0;
    args[0].isWireless = isWireless;
    
    args[1].dev_handle = dev_handle;
    args[1].noteArray = noteArray1;
    args[1].notesCount = notesCount1;
    args[1].channel = 1;
    args[1].isWireless = isWireless;
    
    pthread_create(&threads[0], NULL, playLoop, (void *)&args[0]);
    pthread_create(&threads[1], NULL, playLoop, (void *)&args[1]);
    
    while (g_job0_done == 0 || g_job1_done == 0)
        delay_ms(250);
    
    //Release interface
    cout << "Releasing interface" << endl;
    r = libusb_release_interface(dev_handle,interface_num);
    if(r < 0) {
        cout<<"Interface release Error "<<r<<endl;
        std::cin.ignore();
        return 1;
    }

    libusb_close(dev_handle);
    pthread_exit(NULL);
    
    return 0;
}
